\contentsline {chapter}{\numberline {1}What is Aprx?}{1}
\contentsline {chapter}{\numberline {2}Configuration Examples}{3}
\contentsline {section}{\numberline {2.1}Minimal Configuration of Rx-only I-gate}{3}
\contentsline {section}{\numberline {2.2}Minimal Configuration APRS Digipeater}{4}
\contentsline {section}{\numberline {2.3}Controlling New-n-paradigm}{5}
\contentsline {section}{\numberline {2.4}Filtering at APRS Digipeater}{6}
\contentsline {section}{\numberline {2.5}Combined APRS Digipeater and Rx-iGate}{6}
\contentsline {section}{\numberline {2.6}Doing Transmit-iGate}{7}
\contentsline {section}{\numberline {2.7}Digipeater and Transmit-iGate}{8}
